<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('add', 'Api\UserController@add');
Route::get('display', 'Api\UserController@index');
Route::delete('destroy/{id}', 'Api\UserController@destroy');
Route::put('update/{id}', 'Api\UserController@update');
Route::post('file', 'Api\UserController@fileupload');


// Route::get('display', 'controller@index');
// Route::post('api/a', 'controller@userRegistration');
// Route::get('/api/v1/user/registration', 'controller@userReg');

Route::post('v1/user', 'Controller@userReg');
Route::get('v1/allusers', 'Controller@index');
Route::delete('v1/delete/{id}', 'Controller@destroy');
Route::put('v1/update/{id}', 'Controller@updatedata');
Route::post('v1/fileupload', 'Controller@fileupload');


// Route::get('v1/user/zs','LoginController@userReg');