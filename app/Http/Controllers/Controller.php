<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @SWG\Swagger(
     * host=API_HOST,
     * basePath=API_PATH,
     *     schemes={"http"},
     *     @SWG\Info(
     *         version="1.0.0",
     *         title="L5 Swagger API",
     *         description="L5 Swagger API description",
     *         @SWG\Contact(
     *             email="hactormorris.cis@gmail.com"
     *         ),
     *     )
     * )
     */

    /**
     * @SWG\Get(
     *      path="/projects",
     *      operationId="getProjectsList",
     *      tags={"Projects"},
     *      summary="Get list of projects",
     *      description="Returns list of projects",
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     *
     * Returns list of projects
     */

    /**
     * @SWG\Post(
     *      path="/user",
     *      operationId="registerUser",
     *      tags={"Users"},
     *      summary="Register user",
     *      description="User Registration",
     * @SWG\Parameter(
     *          name="V_name",
     *          in="formData",
     *          required=false,
     *          type="string",
     *          format="string",
     *          description="Username",
     *          default="Harsh",
     *     ),
     *  @SWG\Parameter(
     *          name="V_email",
     *          in="formData",
     *          required=false,
     *          type="string",
     *          format="string",
     *          description="Email",
     *          default="harsh@aa.com",
     *     ),
     * @SWG\Parameter(
     *          name="B_contact",
     *          in="formData",
     *          required=false,
     *          type="number",
     *          format="int32",
     *          description="Contact",
     *          default="77777",
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     */


    public function userReg(Request $request)
    {
        return User::registration($request);
    }

    /**
     * @SWG\Get(
     *      path="/allusers",
     *      operationId="getuserslist",
     *      tags={"Users"},
     *      summary="Get list of users",
     *      description="Returns list of users",
     * @SWG\Response(
     *         response = 200,
     *         description = "Success",
     *         @SWG\Schema(ref="#/definitions/UserResponse"),
     *         examples={
     *              "application/json": {
     *                   "responseCode": 200,
     *                   "responseMessage": "Success",
     *                   "responseData": {},
     *                                   }
     *                   },
     *              ),
     *         )
     *
     * Returns list of users
     */
    public function index()
    {
        return User::getdata();
    }

    /**
     * @SWG\Delete(
     *      path="/delete/{id}",
     *      tags={"Users"},
     *      operationId="deleteuser",
     *      summary="Delete User",
     *      @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          type="integer",
     *          format="int32",
     *          default="1",
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     */
    public function destroy($id)
    {
        return User::deleteuser($id);
    }
    /**
     * @swg\Post(path="/fileupload",
     * tags={"Users"},
     * summary="Upload files",
     * operationId="Upload files",
     * produces={"application/json"},
     * consumes={"multipart/form-data"},
     *
     * @swg\Parameter(
     * description="Upload file",
     * in="formData",
     * name="filename",
     * required=true,
     * type="file",
     * ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @SWG\Response(response=400, description="Bad request"),
     *       security={
     *           {"api_key_security_example": {}}
     *       }
     *     )
     */
    public function fileupload(Request $request)
    {
        return User::fileup($request);
    }
    /**
     * @SWG\Put(
     *      path="/update/{id}",
     *      operationId="UpdateUser",
     *      tags={"Users"},
     *      summary="Update user",
     *      description="User Update",
     *  @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          type="integer",
     *          format="int32",
     *          default="1",
     *     ),
     * @SWG\Parameter(
     *          name="V_name",
     *          in="formData",
     *          required=false,
     *          type="string",
     *          format="string",
     *          description="Username",
     *          default="Harsh",
     *     ),
     *  @SWG\Parameter(
     *          name="V_email",
     *          in="formData",
     *          required=false,
     *          type="string",
     *          format="string",
     *          description="Email",
     *          default="harsh@aa.com",
     *     ),
     * @SWG\Parameter(
     *          name="B_contact",
     *          in="formData",
     *          required=false,
     *          type="number",
     *          format="int32",
     *          description="Contact",
     *          default="77777",
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     */
    public function updatedata(Request $request, $id)
    {
        return User::updaterecord($request, $id);
    }
}
