<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function add(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'contact' => 'required',
        ]);
        if ($valid->fails()) {
            return response()->json(['message' => 'All Fields Required!', 'status' => '404']);
        }
        $user = new User();
        $user->V_name = $request->name;
        $user->V_email = $request->email;
        $user->B_contact = $request->contact;
        $user->save();

        return response()->json(['message' => 'Inserted Successfully!', 'status' => '200']);
    }

    public function index()
    {
        $user = User::get();
        return response()->json(['message' => 'Success!', 'status' => '200', 'data' => $user]);
    }

    public function destroy($id)
    {
        // $user = User::where('id', $id)->first();
        // if (isset($user)) {
        //     $user->delete();
        // } else {
        //     return response()->json(['message' => 'Resource not found', 'status' => '404']);
        // }
        // return response()->json(['message' => 'Deleted!', 'status' => '200']);
        if (User::where('id', $id)->exists()) {
            $user = User::where('id', $id)->first();
            $user->delete();
            return response()->json(['message' => 'Deleted!', 'status' => '200']);
        } else {
            return response()->json(['message' => 'Resource not found', 'status' => '404']);
        }
    }

    public function update(Request $request, $id)
    {
        // dd(request()->name);
        $user = User::where('id', $id)->first();
        if (isset($user)) {
            $user->V_name = $request->name;
            $user->V_email = $request->email;
            $user->B_contact = $request->contact;
            $user->save();

            return response()->json(['message' => 'Your Record Updated!', 'status' => '200', 'data' => $user]);
        } else {
            return response()->json(['message' => 'Resource not found', 'status' => '404']);
        }
    }

    public function fileupload(Request $request)
    {
        $valid = Validator::make($request->all(), [
            // 'image' => 'required|image|mimes:jpeg,png,jpg|max:1024'
            'image' => 'required|mimes:jpeg,png,jpg|max:1024',
        ]);
        if ($valid->fails()) {
            return response()->json(['message' => 'File Format Not Supported!', 'status' => '404']);
        } else {
            $file = $request->file('image');
            $img_name = rand(10000, 99999) . "-" . time() . "." . $file->extension();
            Storage::disk('public')->put('imgs/' . $img_name, file_get_contents($file));
            return response()->json(['message' => 'Your File Uploaded!', 'status' => '200']);
        }
    }
}
