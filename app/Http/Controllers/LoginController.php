<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function create()
    {
        return view('login');
    }

    public function postlogin(Request $request)
    {
        // dd(Hash::make($request->password));
        $auth = auth()->guard('admin');

        if ($auth->attempt($request->only('email', 'password'))) {
            // $request->input('active')
            // return 'You are logged in';
            // return view('dashboard');
            return redirect()->route('dashboard')->with('success', 'You Successfully Logged in!');
        } else {
            return redirect()->route('login')->withErrors('Opps! You have entered invalid credentials');
        }
    }

   
}
