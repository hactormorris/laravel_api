<?php

namespace App\Models\response;

/**
 * @SWG\Definition(
 *   type="object",
 *   @SWG\Xml(name="UserResponseData")
 * )
 */


class UserResponseData
{

    /**
     * @SWG\Property(format="string")
     * @var string
     */
    private $V_name;

    /**
     * @SWG\Property(format="string")
     * @var string
     */
    private $V_email;

    /**
     * @SWG\Property(format="int32")
     * @var int
     */
    private $B_contact;




    function getname()
    {
        return $this->V_name;
    }

    function getemail()
    {
        return $this->V_email;
    }

    function getcontact()
    {
        return $this->B_contact;
    }


    function setname($V_name)
    {
        $this->V_name = $V_name;
    }

    function setemail($V_email)
    {
        return $this->V_email = $V_email;
    }

    function setcontact($B_contact)
    {
        return $this->B_contact = $B_contact;
    }


    public static function withData($V_name, $V_email, $B_contact)
    {
        $instance = new self();

        $instance->setname($V_name);
        $instance->setemail($V_email);
        $instance->setcontact($B_contact);

        return $instance;;
    }

    public function showEverything()
    {
        return get_object_vars($this);
    }
}
