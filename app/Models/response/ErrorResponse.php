<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models\response;

/**
 * @SWG\Definition(
 *   type="object",
 *   @SWG\Xml(name="ErrorResponse")
 * )
 */
class ErrorResponse
{

    /**
     * @SWG\Property(format="int32")
     * @var int
     */
    private $responseCode;

    /**
     * @SWG\Property(format="string")
     * @var string
     */
    private $responseMessage;

    function getResponseCode()
    {
        return $this->responseCode;
    }

    function getResponseMessage()
    {
        return $this->responseMessage;
    }

    function setResponseCode($responseCode)
    {
        $this->responseCode = $responseCode;
    }

    function setResponseMessage($responseMessage)
    {
        $this->responseMessage = $responseMessage;
    }

    public static function withData($responseCode, $responseMessage)
    {
        $instance = new self();
        $instance->setResponseCode($responseCode);
        $instance->setResponseMessage($responseMessage);
        return $instance;
    }

    public function showEverything()
    {
        return get_object_vars($this);
    }
}
