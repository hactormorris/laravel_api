<?php

namespace App\Models;

use App\Models\response\ErrorResponse;
use Illuminate\Database\Eloquent\Model;
use App\Models\response\SuccessResponse;
use App\Models\response\UserResponseData;
use App\Models\response\UserResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'user';

    public $timestamps = false;

    protected $fillable = [
        'id', 'V_name', 'V_email', 'B_contact',
    ];

    // protected $hidden = [
    //     'password'
    // ];

    public static function registration($request)
    {
        $valid = Validator::make($request->all(), [
            'V_name' => 'required|regex:/^[a-zA-Z]+$/u|max:255|', 'V_email' => 'required|email|max:255', 'B_contact' => 'required'
        ]);
        if ($valid->fails()) {
            return ErrorResponse::withData('404', 'Invalid Inputs!')->showEverything();
        } else {
            $user = new user();
            $user->V_name = $request->V_name;
            $user->V_email = $request->V_email;
            $user->B_contact = $request->B_contact;
            $user->save();
            return SuccessResponse::withData('200', 'success')->showEverything();
        }
    }

    public static function getdata()
    {
        $users = User::get();
        $usersdata = [];
        foreach ($users as $user) {
            $usersdata[] = UserResponseData::withdata($user->V_name, $user->V_email, $user->B_contact)->showEverything();
        }
        return UserResponse::withData('200', 'Success', $usersdata)->showEverything();
    }

    public static function deleteuser($id)
    {
        $user = User::where('id', $id)->first();
        if (isset($user)) {
            $user->delete();
            return SuccessResponse::withData('200', 'Deleted Successfully!')->showEverything();
        } else {
            return ErrorResponse::withData('404', 'Resource Not Found!')->showEverything();
        }
    }

    public static function updaterecord($request, $id)
    {

        $valid = Validator::make($request->all(), [
            'V_name' => 'required|regex:/^[a-zA-Z]+$/u|max:255|', 'V_email' => 'required|email|max:255', 'B_contact' => 'required'
        ]);
        if ($valid->fails()) {
            return ErrorResponse::withData('404', 'Invalid Inputs!')->showEverything();
        } else {
            $user = User::where('id', $id)->first();
            if (isset($user)) {
                User::where('id', $id)->update([
                    "V_name" => $request->V_name,
                    "V_email" => $request->V_email,
                    "B_contact" => $request->B_contact
                ]);
                return SuccessResponse::withData('200', 'Your Data Updated!')->showEverything();
            } else {
                return ErrorResponse::withData('404', 'Resource Not Found!')->showEverything();
            }
        }
    }
    public static function fileup($request)
    {
        $valid = Validator::make($request->all(), [
            'filename' => 'mimes:jpeg,png,jpg|max:2048'
        ]);
        if ($valid->fails()) {
            return ErrorResponse::withData('404', 'Invalid File!')->showEverything();
        } else {
            $fileName = $request->file('filename');
            $name = time() . '-' . mt_rand(100000, 999999) . '-' . mt_rand(100000, 999999) . '.' . $fileName->getClientOriginalExtension();
            $path = 'images';
            $filePath = $path . '/' . $name;
            Storage::disk('public')->put($filePath,  file_get_contents($fileName));
            return SuccessResponse::withData('200', 'Your File Uploaded!')->showEverything();
        }
    }
}
